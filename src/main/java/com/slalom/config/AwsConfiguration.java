package com.slalom.config;

import com.netflix.appinfo.AmazonInfo;
import com.slalom.config.aws.Container;
import com.slalom.config.aws.EcsTaskMetadata;
import org.springframework.cloud.commons.util.InetUtils;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AwsConfiguration {

    private final Environment env;

    public AwsConfiguration(Environment env) {
        this.env = env;
    }

    @Bean
    @Profile("prod")
    public EurekaInstanceConfigBean eurekaInstanceConfig(InetUtils inetUtils) {
        EurekaInstanceConfigBean config = new EurekaInstanceConfigBean(inetUtils);
        AmazonInfo info = AmazonInfo.Builder.newBuilder().autoBuild("eureka");
        config.setDataCenterInfo(info);

        RestTemplate template = new RestTemplate();
        EcsTaskMetadata result = template.getForObject("http://169.254.170.2/v2/metadata", EcsTaskMetadata.class);

        String ipAddress = "0.0.0.0";
        for (Container c : result.getContainers()) {
            if (c.getName().toLowerCase().contains("node")) {
                ipAddress = c.getNetworks()[0].getIPv4Addresses()[0];
            }
        }
        config.setIpAddress(ipAddress);
        config.setNonSecurePort(getPortNumber());

        return config;
    }

    private int getPortNumber() {
        return env.getProperty("server.port", Integer.class);
    }
}
