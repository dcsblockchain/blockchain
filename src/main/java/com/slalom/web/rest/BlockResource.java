package com.slalom.web.rest;

import com.slalom.blockchain.Block;
import com.slalom.blockchain.BlockChain;
import com.slalom.blockchain.TitleTransferData;
import com.slalom.service.BlockService;
import com.slalom.web.dto.BlockHeadDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/block")
public class BlockResource {
    private final Logger log = LoggerFactory.getLogger(BlockResource.class);

    private final BlockChain blockChain;
    private final BlockService blockService;

    public BlockResource(Environment environment, BlockService blockService) {
        blockChain = new BlockChain(environment);
        this.blockService = blockService;
    }

    @GetMapping("/head")
    public ResponseEntity<BlockHeadDTO> getBlockChainIndex() {
        log.debug("Processing request to get the blockchain head");
        return ResponseEntity.ok().body(blockChain.getHead());
    }

    @GetMapping("/get-from/{index}")

    public ResponseEntity<List<Block>> getBlockChainIndex(@PathVariable long index) {
        log.debug("Processing request to get the blocks from index {}", index);
        return ResponseEntity.ok().body(blockChain.getFrom(index));
    }

    @PostMapping
    public ResponseEntity addTransaction(@RequestBody TitleTransferData transaction) {
        log.debug("Processing request to add a new transaction to the blockchain");
        blockService.createNewBlock(transaction);
        return ResponseEntity.ok().build();
    }
}
