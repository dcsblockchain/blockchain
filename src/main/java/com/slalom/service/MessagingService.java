package com.slalom.service;

import com.slalom.blockchain.Block;
import com.slalom.blockchain.BlockChain;
import com.slalom.messaging.BlockConsumerChannel;
import com.slalom.messaging.BlockProducerChannel;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.core.env.Environment;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

@Service
public class MessagingService {

    private MessageChannel messageChannel;
    private Environment environment;

    public MessagingService(BlockProducerChannel blockProducerChannel, Environment environment) {
        this.messageChannel = blockProducerChannel.blockCreated();
        this.environment = environment;
    }

    public void publishBlock(Block block) {
        messageChannel.send(MessageBuilder.withPayload(block).build());
    }

    // TODO stop mining when another miner finds the nonce first
    @StreamListener(BlockConsumerChannel.CHANNEL)
    public void minedBlockReceived(Block block) {
        BlockChain blockChain = new BlockChain(environment);
        blockChain.addBlock(block);
    }
}
