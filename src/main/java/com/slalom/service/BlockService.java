package com.slalom.service;

import com.slalom.blockchain.Block;
import com.slalom.blockchain.BlockChain;
import com.slalom.blockchain.TitleTransferData;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BlockService {
    private final BlockChain blockChain;
    private final MessagingService messagingService;

    public BlockService(Environment environment, MessagingService messagingService) {
        blockChain = new BlockChain(environment);
        this.messagingService = messagingService;
    }

    public void createNewBlock(TitleTransferData data) {
        Block head = blockChain.getHead().getHead();
        Block block = new Block();
        block.setIndex(head.getIndex() + 1);
        block.setPreviousHash(head.getHash());
        block.setData(data);
        block.setTimeStamp(new Date().getTime());
        messagingService.publishBlock(block);
    }
}
