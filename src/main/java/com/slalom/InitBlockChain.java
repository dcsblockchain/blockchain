package com.slalom;

import com.google.inject.Singleton;
import com.slalom.blockchain.Block;
import com.slalom.blockchain.BlockChain;
import com.slalom.service.MessagingService;
import com.slalom.web.dto.BlockHeadDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.event.EventListener;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
@Singleton
public class InitBlockChain {

    private DiscoveryClient discoveryClient;
    private Environment environment;
    private int count = 0;
    private MessagingService messagingService;

    @Autowired
    public void setBlockProducerChannel(MessagingService messagingService) {
        this.messagingService = messagingService;
    }

    @Autowired
    public void setDiscoveryClient(DiscoveryClient discoveryClient) {
        this.discoveryClient = discoveryClient;
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

   @EventListener(ApplicationReadyEvent.class)
    public void applicationStarted() {
       if (count > 0) {
           initBlockChain();
       }
       count++; // reactor library is throwing this event first too early in the process
    }


    private void initBlockChain() {
        BlockChain blockChain = new BlockChain(environment); // creates a new blockchain if needed
        if (isThisTheOnlyNode()) {
            if (blockChain.getHead().getHead() == null) {
                Block genesis = blockChain.createGenesisBlock();
                messagingService.publishBlock(genesis);
            }
        } else {
            syncNode(blockChain);
        }
    }

    private void syncNode(BlockChain blockChain) {
        BlockHeadDTO blockHead = blockChain.getHead();
        long currentChainSize = blockHead.getBlockSize();
        long currentChainTimestamp = blockHead.getHead() != null ? blockHead.getHead().getTimeStamp() : 0;
        ServiceInstance mostRecentInstance = null;
        RestTemplate restTemplate = new RestTemplate();

        // iterate through the instances of nodes to find the most recent one
        for (ServiceInstance instance : discoveryClient.getInstances("node")) {
            try {
                ResponseEntity<BlockHeadDTO> response = restTemplate.getForEntity("http://" + instance.getHost() + ":" + instance.getPort() + "/block/head", BlockHeadDTO.class);
                // the response received has a longer and more recent chain, it should use it
                if (response.getBody() != null && response.getBody().getBlockSize() > currentChainSize && response.getBody().getHead().getTimeStamp() > currentChainTimestamp) {
                    mostRecentInstance = instance;
                }
            } catch (Exception e) {
                System.out.println("Failed to contact node");
            }
        }
        if (mostRecentInstance != null) {
            ResponseEntity<List<Block>> listOfBlocks = restTemplate.exchange("http://" + mostRecentInstance.getHost() + ":" + mostRecentInstance.getPort() + "/block/get-from/" + currentChainSize,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Block>>() {});
            if (listOfBlocks.getBody() != null) {
                listOfBlocks.getBody().forEach(blockChain::addBlock);
            }
        }
    }

    // asks the network (service discovery) if there's an existing chain
    private boolean isThisTheOnlyNode() {
        List<ServiceInstance> instances = discoveryClient.getInstances("node");
        return instances.size() == 0;
    }
}
