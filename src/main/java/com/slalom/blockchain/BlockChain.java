package com.slalom.blockchain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Singleton;
import com.slalom.web.dto.BlockHeadDTO;
import org.springframework.core.env.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Singleton
public class BlockChain {
    private File blockChain;
    private static Block headOfChain;
    private ObjectMapper objectMapper = new ObjectMapper();

    public BlockChain(Environment environment) {
        String filePath = environment.getProperty("blockchain-path") != null ? environment.getProperty("blockchain-path") : "blockchain.chain";
        blockChain = new File(filePath);
        try {
            if (!blockChain.createNewFile() && headOfChain == null) {
                // file already exists and the head is null need to update the headOfChain
                long size = Files.lines(blockChain.toPath()).count();
                if (size > 0) {
                    headOfChain = objectMapper.readValue(Files.lines(blockChain.toPath()).skip(size - 1).findFirst().get(), Block.class);
                }
            }
        } catch (IOException e) {
            System.out.println("Unable to create file");
            e.printStackTrace();
        }
    }

    public Block createGenesisBlock() {
        Block block = new Block();
        TitleTransferData genesis = new TitleTransferData();
        String GENESIS_BLOCK = "Genesis Block";
        genesis.setBuyerFirstName(GENESIS_BLOCK);
        genesis.setBuyerLastName(GENESIS_BLOCK);
        genesis.setSellerFirstName(GENESIS_BLOCK);
        genesis.setSellerLastName(GENESIS_BLOCK);
        genesis.setAddress(GENESIS_BLOCK);
        genesis.setCounty(GENESIS_BLOCK);
        genesis.setSaleContractDate(new Date());
        genesis.setTelephoneNumber("0000000000");
        genesis.setSalesPrice(new BigDecimal(0));
        genesis.setMake(GENESIS_BLOCK);
        genesis.setModel(GENESIS_BLOCK);
        genesis.setBodyType(GENESIS_BLOCK);
        genesis.setColor(GENESIS_BLOCK);
        genesis.setYear(1900);
        genesis.setBuyerId(0L);
        block.setData(genesis);
        block.setPreviousHash("0000000000000000000000000000000000000000000000000000000000000000");
        block.setTimeStamp(new Date().getTime());
        return block;
    }

    public BlockHeadDTO getHead() {
        BlockHeadDTO head = new BlockHeadDTO();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(blockChain));
            head.setBlockSize(reader.lines().count());
            head.setHead(headOfChain);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return head;
    }

    public List<Block> getFrom(long index) {
        List<Block> result = new ArrayList<>(); // result has to maintain order
        try {
            Files.lines(blockChain.toPath()).skip(index).forEach(line -> {
                try {
                    result.add(objectMapper.readValue(line, Block.class));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void addBlock(Block block) {
        try {
            long chainSize = Files.lines(blockChain.toPath()).count();

            Optional<String> blockFound = Files.lines(blockChain.toPath()).skip(block.getIndex()).findFirst();
            if (blockFound.isPresent()) {
                // there's already a block at that position
                Block blockFoundConverted = objectMapper.readValue(blockFound.get(), Block.class);
                if (!blockFoundConverted.getHash().equals(block.getHash())) {
                    throw new RuntimeException("Block can't be added as index and hash don't match");
                }
            } else {
                if (block.getIndex() == 0L && block.isValid()) {
                    BufferedWriter writer = new BufferedWriter(new FileWriter(blockChain));
                    writer.write(objectMapper.writeValueAsString(block));
                    writer.newLine();
                    writer.close();
                    headOfChain = block;
                } else {
                    // add the block to the tail of the chain if valid
                    Optional<String> currentBlock = Files.lines(blockChain.toPath()).skip(chainSize - 1).findFirst();
                    if (currentBlock.isPresent()) {
                        Block blockFoundConverted = objectMapper.readValue(currentBlock.get(), Block.class);
                        // if the previous block exists and the hash matches the previous hash on the current block
                        // then add it
                        if (block.isValid() && blockFoundConverted.getHash().equals(block.getPreviousHash())) {
                            BufferedWriter writer = new BufferedWriter(new FileWriter(blockChain, true));
                            writer.write(objectMapper.writeValueAsString(block));
                            writer.newLine();
                            writer.close();
                            headOfChain = block;
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
