package com.slalom.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface BlockConsumerChannel {
    String CHANNEL = "minedBlockReceived";

    @Input
    SubscribableChannel minedBlockReceived();
}
